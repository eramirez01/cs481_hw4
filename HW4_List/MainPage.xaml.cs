﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace HW4_List
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<Game> GameList
        {
            get;
            set;
        }
        public MainPage()
        {
            InitializeComponent();
            Populate();
        }

        private void Populate()
        {
            GameList = new ObservableCollection<Game>()
            {
                new Game()
                {
                    Name = "Super Smash Bros. Ultimate",
                    System = "Nintendo Switch",
                    Image = "Smash"
                },

                new Game()
                {
                    Name = "Castlevania: Symphony of the Night",
                    System = "Playstation",
                    Image = "Symphony"
                },

                new Game()
                {
                    Name = "Tekken 7",
                    System = "Playstation 4",
                    Image = "Tekken"
                },

                new Game()
                {
                    Name = "The Outer Worlds",
                    System = "Xbox One",
                    Image = "Outer"
                },
            };

            GameListView.ItemsSource = GameList;
        }

        void Handle_Refreshing(object sender, EventArgs e)
        {
            Populate();
            GameListView.IsRefreshing = false;
        }

        void Handle_DeleteGame(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var gameDelete = (Game)menuItem.CommandParameter;
            GameList.Remove(gameDelete);
        }
    }
}
