﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace HW4_List
{
    public class Game
    {
        public string Name
        {
            get;
            set;
        }

        public string System
        {
            get;
            set;
        }

        public ImageSource Image
        {
            get;
            set;
        }
    }
}